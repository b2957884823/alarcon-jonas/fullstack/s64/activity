let collection = [];

function print() {
    return collection;
}

function enqueue(element) {
    collection.push(element);
    return collection;
}

function dequeue() {
    if (collection.length === 0) {
        return [];
    }
    collection.shift();
    return collection;
}

function front() {
    return collection.length === 0 ? undefined : collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}



module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
